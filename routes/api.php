<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
 return $request->user();
});


/************ CATEGORY ************/
Route::group(['prefix' => 'category'], function () {
   Route::get('index', [App\Http\Controllers\Category\CategoryController::class, 'index'])->name('category.index');
});

/************ AUTHENTICATION ************/
Route::group(['prefix' => 'auth'], function () {
  Route::post('/register','App\Http\Controllers\Auth\AuthController@register')->name('register.api');
  Route::post('/login','App\Http\Controllers\Auth\AuthController@login')->name('login.api');
});

Route::middleware('auth:sanctum')->group(function(){
   /************ PROFILE ************/
   Route::get('/user','App\Http\Controllers\Auth\AuthController@userdata')->name('user.api');
   Route::get('/get_photografer','App\Http\Controllers\Photo\PhotoController@get_photografer')->name('user.photografer');
   
   Route::post('/get_photografer', [App\Http\Controllers\Photo\PhotoController::class, 'get_photografer'])->name('user.get.get_photografer');

   /************ AVATAR ************/
   Route::post('/avatar','App\Http\Controllers\Auth\AuthController@avatar')->name('avatar.api');
   
   /************ PROFILE UPDATE ************/
   Route::post('/profile','App\Http\Controllers\Auth\AuthController@updateUser')->name('updateUser.api');

   /************ BIO UPDATE ************/
   Route::post('/bio','App\Http\Controllers\Auth\AuthController@updateBio')->name('updateBio.api'); 

   /************ PHOTO ************/
   Route::group(['prefix' => 'photo'], function () {
      Route::post('add', [App\Http\Controllers\Photo\PhotoController::class, 'store'])->name('photo.add.store');
      Route::post('get_photo_by_user_id', [App\Http\Controllers\Photo\PhotoController::class, 'get_photo_by_user_id'])->name('photo.get.get_photo_by_user_id');
      Route::post('getPhotofByCategory', [App\Http\Controllers\Photo\PhotoController::class, 'getPhotofByCategory'])->name('photo.get.getPhotofByCategory');

      Route::post('get_photo_by_category_id', [App\Http\Controllers\Photo\PhotoController::class, 'get_photo_by_category_id'])->name('photo.get.get_photo_by_category_id');
   });


   /************ REVIEW ************/
   Route::group(['prefix' => 'review'], function () {
      Route::post('add', [App\Http\Controllers\Review\ReviewController::class, 'postReview'])->name('review.postReview');
      Route::post('get_review_by_photografer', [App\Http\Controllers\Review\ReviewController::class, 'get_review_by_photografer'])->name('review.get_review_by_photografer');
      Route::post('get_review_by_user_id', [App\Http\Controllers\Review\ReviewController::class, 'get_review_by_user_id'])->name('review.get_review_by_user_id');
   });

   /************ ORDER ************/
   Route::group(['prefix' => 'order'], function () {
      Route::post('add', [App\Http\Controllers\Order\OrderController::class, 'store'])->name('order.store');
      Route::post('get_order_by_user_id', [App\Http\Controllers\Order\OrderController::class, 'get_order_by_user_id'])->name('order.get_order_by_user_id');
      Route::post('get_order_by_photof', [App\Http\Controllers\Order\OrderController::class, 'get_order_by_photof'])->name('order.get_order_by_photof');
      Route::post('status', [App\Http\Controllers\Order\OrderController::class, 'status'])->name('order.status');


   });



});