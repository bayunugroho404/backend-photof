<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    use HasFactory;
    protected $fillable = [
        'rate',
        'user_id',
        'description',
        'photografer_id'
    ];

    public function user()
    {
        return $this->hasOne(User::class,'id','user_id');
    }
    
    public function photografer()
    {
        return $this->hasOne(User::class,'id','photografer_id');
    }

}
