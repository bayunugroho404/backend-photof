<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Photo extends Model
{
    use HasFactory;


    protected $fillable = [
        'user_id',
        'photo'
    ];
    public function getPhotoAttribute($name)
    {
        return asset('storage/uploads/' . $name);
    }


    public function user()
    {
        return $this->hasOne(User::class,'id','user_id');
    }

}
