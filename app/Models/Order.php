<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;

    protected $fillable = [
        'date','packet','user_id','photografer_id','note','status'
    ];

    public function user()
    {
        return $this->hasOne(User::class,'id','user_id');
    }
    
    public function photografer()
    {
        return $this->hasOne(User::class,'id','photografer_id');
    }

}
