<?php

namespace App\Http\Controllers\Order;

use App\Http\Controllers\Controller;
use App\Models\Order;
use Illuminate\Http\Request;

class OrderController extends Controller
{
  public function store(Request $request){
    $data = $request->all();
    $project = Order::create($data);
    return response()->json([
        'status_code' => 200,
        'message' => 'successfull order'
    ]);
}

public function get_order_by_photof(Request $request){
    try{
        $data=  Order::orderBy('created_at', 'desc')->with('user','photografer')->where('photografer_id',$request->photografer_id)->get();
        return response()->json([
            'status_code' => 200,
            'orders' => $data
        ]);
    }catch (\Exception $e) {
        return response()->json([
            'message' => $e,
        ]);
    }
}

public function get_order_by_user_id(Request $request){
    try{
        $data=  Order::orderBy('created_at', 'desc')->with('user','photografer')->where('user_id',$request->user_id)->get();
        return response()->json([
            'status_code' => 200,
            'orders' => $data
        ]);
    }catch (\Exception $e) {
        return response()->json([
            'message' => $e,
        ]);
    }
}


public function status(Request $request){
  try{
    $order_id = $request->order_id;
    $status = $request->status;

    $order = Order::find($order_id);
    $order->status = $status;
    $order->save();

    return response()->json([
        'status_code' => 200,
        'message' => 'successfull change status order'
    ]);
}catch (\Exception $e) {
    return response()->json([
        'message' => $e,
    ]);
}       
}
}
