<?php

namespace App\Http\Controllers\Review;

use App\Http\Controllers\Controller;
use App\Models\Review;
use Illuminate\Http\Request;

class ReviewController extends Controller
{
    public function postReview(Request $request){
        $data = $request->all();
        // $review = Review::get();
        $project = Review::create($data);
        return response()->json([
            'status_code' => 200,
            'reviews' => 'successfull review'
        ]);
    }

    public function get_review_by_photografer(Request $request){
        try{
            $reviews=  Review::with('user','photografer')->where('photografer_id',$request->photografer_id)->get();
            return response()->json([
                'status_code' => 200,
                'reviews' => $reviews
            ]);
        }catch (\Exception $e) {
            return response()->json([
                'message' => $e,
            ]);
        }
    }


    public function get_review_by_user_id(Request $request){
        try{
            $reviews=  Review::with('user','photografer')->where('user_id',$request->user_id)->get();
            return response()->json([
                'status_code' => 200,
                'reviews' => $reviews
            ]);
        }catch (\Exception $e) {
            return response()->json([
                'message' => $e,
            ]);
        }
    }



}
