<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class AuthController extends Controller
{
   public function login(Request $request){
    try{
        $request->validate([
            'email' => 'email|required',
            'password' => 'required',
        ]);

        $credentials = request(['email', 'password']);

        if (!Auth::attempt($credentials)){
            return response()->json([
                'status_code' => 422,
                'message' => 'Unauthorized',
                
            ]);
        }

        $user =  User::where('email', $request->email)->first();
        if(!Hash::check($request->password, $user->password, [])){
            return response()->json([
                'status_code' => 422,
                'message' => 'Password Match',
                
            ]);
        }

        $tokenResult = $user->createToken('authToken')->plainTextToken;
        return response()->json([
            'status_code' => 200,
            'access_token' => $tokenResult,
            'user' => $user,
            'token_type' => 'Bearer',
        ]);

    }catch(Exception $error){
        return response()->json([
            'status_code' => 500,
            'message' => 'Error in login',
            'error' => $error,
        ]);
    }
}

public function register(Request $request){
    try {

        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'phone_number' => 'required',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);

        if($validator->fails()){
            return response([
                'error' => $validator->errors()->all()
            ], 422);
        }

        $request['password'] = Hash::make($request['password']);
        $request['remember_token'] = Str::random(10);
        $user = User::create($request->toArray());

        return response()->json([
            'status_code' => 200,
            'message' => 'Registration Successfull',
        ]);


    }catch(Exception $error){
        return response()->json([
            'status_code' => 500,
            'message' => 'Error in Registration',
            'error' => $error,
        ]);
    }
}

public function userdata(Request $request){
    return $request->user();
}

public function logout(){
    Auth::user()->token()->delete();
    return response()->json([
        'status_code' => 200,
        'message' => 'Logout successfull',
    ]);
}


public function avatar(Request $request)
{
    try {
        $avatar                   = $request->file('avatar')->store(null, 'uploads');
        $user                     = User::find(Auth::id());
        $user->photo             = $avatar;
        $user->save();

        return response()->json([
            'status_code' => 200,
            'message' => 'OK',
            'user'=> $user
        ]);


    } catch (\Exception $e) {
        return $e;
    }
}

public function updateUser(Request $request)
{
    try {
        $name                   = $request->name;
        $email                   = $request->email;
        $no                   = $request->no;
        
        $user                     = User::find(Auth::id());
        $user->name             = $name;
        $user->email             = $email;
        $user->phone_number       = $no;
        $user->save();
        
        return response()->json([
            'status_code' => 200,
            'message' => 'OK',
        ]);


    } catch (\Exception $e) {
        return $e;
    }
}
public function updateBio(Request $request)
{
    try {
        $description                   = $request->description;
     
        $user                     = User::find(Auth::id());
        $user->description             = $description;
    
        $user->save();
        
        return response()->json([
            'status_code' => 200,
            'message' => 'OK',
        ]);


    } catch (\Exception $e) {
        return $e;
    }
}
}
