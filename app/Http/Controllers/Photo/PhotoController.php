<?php

namespace App\Http\Controllers\Photo;

use App\Models\Photo;
use Illuminate\Support\Facades\Validator;
use App\Models\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PhotoController extends Controller
{
    public function store(Request $request)
    {
        try {
            if ($file = $request->file('photo')) {
                $path =$request->file('photo')->store(null, 'uploads');
                
                $save = new Photo();
                $save->photo = $path;
                $save->user_id= $request->user_id;
                $save->save();

                return response()->json([
                    "success" => true,
                    "message" => "File successfully uploaded",
                    "file" => $path
                ]);

            }


        } catch (\Exception $e) {
            return $e;
        }
    }

    public function get_photo_by_user_id(Request $request){
        try{

            //update click
            $user                     = User::find($request->user_id);
            $user->click               = $user->click+1;
            $user->save();

            $photo  = Photo::with('user')
            ->where('user_id',$request->user_id)->get();


            return response()->json([
                'status_code' => 200,
                'photo' => $photo,
            ]);
        }catch (\Exception $e) {
            return response()->json([
                'message' => $e,
            ]);
        }
    }


    public function get_photografer(Request $request){
        try{
            $photo  = User::with('photos')->where('is_user',0)->get();

            return response()->json([
                'status_code' => 200,
                'photo' => $photo,
            ]);
        }catch (\Exception $e) {
            return response()->json([
                'message' => $e,
            ]);
        }
    }

    public function getPhotofByCategory(Request $request){
        try{
            $category_id = $request->id;
            $validator = Validator::make($request->all(), [
                'id' => 'required',
            ]);

            if($validator->fails()){
                return response([
                    'error' => $validator->errors()->all()
                ], 422);
            }
            $photo  = User::with('photos')->where('is_user',0)
            ->where('category_id',$category_id)
            ->get();
            
            return response()->json([
                'status_code' => 200,
                'photografer' => $photo,
            ]);
        }catch (\Exception $e) {
            return response()->json([
                'message' => $e,
            ]);
        }
    }


    public function get_photo_by_category_id(Request $request){
        try{
            $photo  = Photo::with('user','category')->where('category_id',$request->category_id)->get();

            return response()->json([
                'status_code' => 200,
                'photo' => $photo,
            ]);
        }catch (\Exception $e) {
            return response()->json([
                'message' => $e,
            ]);
        }
    }

}
