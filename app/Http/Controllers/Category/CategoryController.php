<?php

namespace App\Http\Controllers\Category;

use App\Http\Controllers\Controller;
use App\Models\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{

    public function index(){
    try{
        $photo  = Category::get();

        return response()->json([
            'status_code' => 200,
            'data' => $photo,
        ]);
    }catch (\Exception $e) {
        return response()->json([
            'message' => $e,
        ]);
    }
}
}
