<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Category;

class CatergorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
     $category = [
        [
         'name' => 'Birthday',
     ], 
     [
         'name' => 'Wedding',
     ], 
     [
         'name' => 'Product',
     ], 

     [
         'name' => 'Other',
     ], 
 ];

 foreach ($category as $key => $value) {
     Category::create($value);
 }
}
}
