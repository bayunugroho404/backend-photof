<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class CreateUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = [
        	[
             'name' => 'photof1',
             'email' => 'photof1@gmail.com',
             'is_user' => 0,
             'description' => 'Im photographer proffesional',
             'work_schedule' => 'senin - jumat',
             'category_id' => 1,
             'phone_number' => "082121321321",
             'password' => bcrypt('password')
         ], 
         [
             'name' => 'photof',
             'email' => 'photof2@gmail.com',
             'is_user' => 0,
             'category_id' => 1,

             'description' => 'I have 5 years of experience as a photographer',
             'work_schedule' => 'everyday',
             'phone_number' => "082999999",
             'password' => bcrypt('password')
         ], 
         [
             'name' => 'photof3',

             'category_id' => 2,
             'email' => 'photof3@gmail.com',
             'is_user' => 0,
             'description' => 'Im photographer proffesional',
             'work_schedule' => 'senin - jumat',
             'phone_number' => "082121321321",
             'password' => bcrypt('password')
         ], 
         [
             'name' => 'photof',
             'email' => 'photof4@gmail.com',
             'is_user' => 0,
             'category_id' => 3,

             'description' => 'I have 5 years of experience as a photographer',
             'work_schedule' => 'everyday',
             'phone_number' => "082999999",
             'password' => bcrypt('password')
         ], 
         [
             'name' => 'user',
             'email' => 'user@gmail.com',
             'is_user' => 1,
             'phone_number' => "082121321321",
             'password' => bcrypt('password')
         ], 
     ];

     foreach ($user as $key => $value) {
         User::create($value);
     }

 }
}
